
package javaapplication1;

/**
 *
 * @author krotxe
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    
    
    //LOGIC1
    
    
    
    
    //1
    //El método devuelve true si, "isWeekend" es igual a "true" y "cigars" 
    //mayor que 40, o si "cigars"  es mayor que 39 y menor de 61, si no es así
    //devulve false.
    public boolean cigarParty(int cigars, boolean isWeekend) {
 
 if (((isWeekend == true)&& (cigars>40))||((cigars>39)&&(cigars<61))){
 return true;}
 return false;
}

    
    
    
    
 
    
    //2
    //El método devulve 1 a no ser que "you" o "date" valgan mas de 7 o menos de 3
    //en cuyo caso devulve 2 o 0 respectivamente.
    public int dateFashion(int you, int date) {
  
  if (you<3||date<3)
  return 0;
  if (you>7||date>7)
  return 2;
  return 1;
}

    
    
    
    //3
    //El método devulve "false" si "temp" es menor que 60, devuelve "true" si 
    //"isSummer" es igual a "true" y "temp" es menor que 101, o bien si "isSummer"
    // es igual a "false"  y "temp" es menor de 91.
    //En los demas casoso devuelve false.
    public boolean squirrelPlay(int temp, boolean isSummer) {

if (temp<60){
return false;
}
 if (((isSummer == true)&& (temp<101))||((isSummer == false)&& (temp<91))){
 return true;}
 return false;
}
    
    
    
    //4
    //Partiendo de que si "isBirthday" es igual a true "spped" pierde 5 en su valor,
    //si "speed" es menor que 61, el método devuelve 0,
    //si "speed" es menor que 81, el método devuelve 1,
    //si "speed" es mayor que 81, el método devuelve 2.
    //En cualquier otro caso el método se demonizará.
public int caughtSpeeding(int speed, boolean isBirthday) {
if (isBirthday==true)
speed = speed-5;
if(speed<61)
return 0;
if(speed<81)
return 1;
if(speed>81)
return 2;
return 666;
}


    


    //5
//Si "a"+"b" es mayor que 9 y menor que 20, el método devuelve 20.
//En cualquier otro caso devuelve la suma de "a" y "b".
public int sortaSum(int a, int b) {
if (((a+b)>9)&&((a+b)<20))
return 20;
return a+b;
}

    
    
    
    
//6
//Si "vacation" es igual a true y "day" el igual a 0 o a 6 devuelve "off", si no 
//devuelve "10:00".
//Por otro lado, si "vacation" es igual a false y "day" el igual a 0 o a 6 
//devuelve "10:00", si no devuelve "7:00".

public String alarmClock(int day, boolean vacation) {

if (vacation==true){
if((day==0)||(day==6)){
    
String ring ="off";
return ring;
}  
String ring ="10:00";
return ring;
}
if((day==0)||(day==6)){
    
String ring ="10:00";
return ring;
}  
String ring ="7:00";
return ring;
}

    
    
//7
//Si "a" o "b" son igual a 6, o su suma lo es, o lo es su diferencia el método
//devuelve true.
//En caso contratio devuelve false.
public boolean love6(int a, int b) {
if ((a==6)||(b==6)||((a+b)==6)||((a-b)==6)||((b-a)==6))
return true;
return false;
}

    
    
    
    
   //8
   //Si "outsideMode" es igual a false y "n" tiene un valor mayor que 0
   //y menor que 11 devuelve true, si "outsideMode" es igual a true y "n"
   //tiene un valor menor que 2 y mayor que 10 también devuelve true.
   //En los demas casos devuelve false.
    public boolean in1To10(int n, boolean outsideMode) {
    
if ((outsideMode==false)&&((n>0)&&(n<11)))
return true;
if ((outsideMode==true)&&((n<2)||(n>=10)))
return true;

return false;
}
    
    
    
    //9
    //Si el resto de "n" entre 11 es 0 o el de "n"-1 entre 11 es igual a 0
    //retorna true, si no retorna false.
    
    public boolean specialEleven(int n) {
if ((n%11==0)||((n-1)%11==0))
return true;
return false;
  
}

    
    
    
    //10
    //Si el resto de "n" -1 o -2 entre 20 es igual a 0 devuelve true,
    //si no devuelve false.
    public boolean more20(int n) {
if (((n-1)%20==0)||((n-2)%20==0))
return true;
return false;
  
}

    
    
    //11
    //Si el resto de "n" entre 3 es 0 y no es 0 el de "n" entre 5,
    //o viceversa, devuelve true, si no devuelve false.
    public boolean old35(int n) {
  if(((n%3==0)&&(n%5!=0))||((n%5==0)&&(n%3!=0)))
  return true;
  return false;
}

    
    
    //12
    //Si el resto de "n"+1 o +2, dividido por 20 es igual a 0 devuelve true
    //si no devuelve false.
    public boolean less20(int n) {
  if(((n+1)%20==0)||((n+2)%20==0))
  return true;
  return false;
}

    
    
    //13
    //Si el resto de dividir "num" entre 10 es igual a 2,1 o 0, o bien 
    //el resto de "num"+1 o +2 entre 10 es 0 devuelve true, si no devuelve false.
    public boolean nearTen(int num) {
if ((num%10==2)||(num%10==1)||(num%10==0)||((num+1)%10==0)||((num+2)%10==0))
return true;
return false;
  
}
    
    
    
    //14
    //Si "a" o "b" tienen valores comprendidos entre 12 y 20 (ambos no inclusives)
    //devuelve 19, si no devuelve su suma.
    public int teenSum(int a, int b) {
if (((a>12)&&(a<20))||((b>12)&&(b<20)))
return 19;
return a+b;
}
    
    
    
    
    //15
    //Si "isAsleep" es igual a true devuelve false.
    //Si "isMom" es igual a true devuelve true
    //Si "isMorning"es igual a true devuelve false.
    //Para todo lo demas mastercard
public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
if (isAsleep==true)
return false;
if (isMom==true)
return true; 

if (isMorning==true)
return false;
return true;
}







    //16
    //Si "tea" o "candy" son menores de 5 devuelve 0.
    //Si alguno de los dos es al menos el doble que el otro devuelve 2.
    //Si valen al menos 4 devuelve 1.
public int teaParty(int tea, int candy) {
if ((tea<5)||(candy<5))
  return 0;
 if ((tea>=(candy*2))||(candy>=(tea*2)))
  return 2;
  
  if ((tea>4)&&(candy>4))
  return 1;
 
  return 666;
}
    



//17
//Si la primera letra de el string "str" es f devuelve Fizz
//Si la última letra de el string "str" es b devuelve Buzz
//Si se cumplen a la vez los dos casos anteriores devuelve FizzBuzz
//Si no se cumple ningun caso devuelve el string str
public String fizzString(String str) {
    String fuzz="Fizz";
    String buzz="Buzz";
    String fb="FizzBuzz";
    
  if ((str.startsWith("f"))&&(str.endsWith("b")))
    return fb;

  if (str.startsWith("f"))   
    return fuzz;

  if (str.endsWith("b"))
    return buzz;
   
return str;
}



//18
//El método devuelve Fizz si "n" es multiplo de 3, Buzz si es multiplo de 5
//y FizzBuzz si es multiplo de ambos.
//Si no se da ningun caso devuelve "n".
//Termina cada devolución con una exclamación.
public String fizzString2(int n) {

String f="Fizz";
String b="Buzz";
String fb="FizzBuzz";
String k="!";

if ((n%3==0)&&(n%5==0))
return fb+k;


if(n%3==0)
return f+k;

if(n%5==0)
return b+k;

return String.valueOf(n)+k;

}




//19
//El método devuelve true si la suma de dos de los valores "a", "b" y "c"
// da como resultado el tercero.
public boolean twoAsOne(int a, int b, int c) {
  
  if ((a+b==c) || (b+c==a) || (c+a==b))
  return true;
  return false;
  
}



//20
//Si "c" es mayor que "b" y éste es mayor que "a" devuelve true.
//Si "bOk" es igual a true y "c" sigues siendo mayor que "b" devuelve true;
//En otros casos devuelve false
public boolean inOrder(int a, int b, int c, boolean bOk) {

if ((c>b)&&(b>a))
return true;
if ((bOk == true)&&(c>b))
return true;
return false;
}




//21
//Si "a" es mayor que "b" y éste es mayor que "c" devuelve true, y si "equalOk"
//es igual a true pueden ser iguales o mayores y seguirá devolviendo true.
//En otro caso devuelve false.
public boolean inOrderEqual(int a, int b, int c, boolean equalOk) {
  
  if((a<b)&&(b<c))
  return true;
  if ((equalOk==true)&&(a<=b)&&(b<=c))
  return true;
  return false;
}







//22
//El método primero consigue la decena en la que se encuentra cada número dividiendo por diez
//y posteriormente multiplicando por diez, y los nombra igual que cada variable pero en mayusculas.
//Después consigue el último digito restando a "a" su decena "A" y así con todos.
//Si al comparar los ultimos digitos de dos en dos encuentra coincidencia devuelve true, si no
//devuelve false.
public boolean lastDigit(int a, int b, int c) {
 int A=(a/10)*10;
 int B=(b/10)*10;
 int C=(c/10)*10;
 
 if (((a-A)==(b-B))||((b-B)==(c-C))||((a-A)==(c-C)))
 return true;
 return false;
}





//23
//Este método comprueba si entre dos de las variables dadas existe 10 de diferencia
//restando a una de ellas 10 y comparandola con otra.Así con todas las opciones.
//Si es cierto devuelve true, si no devuelve false.
public boolean lessBy10(int a, int b, int c) {
if(((a-10)>=b)||((b-10)>=c)||((c-10)>=a)||((b-10)>=a)||((c-10)>=b)||((a-10)>=c))
return true;
return false;
  
}


//24
//El método comprueba si "noDoubles" es igual a true, si es así comprueba si "die1"
//y "die2" son iguales, si es así suma 1 al valor de "die1" en el caso de que no tenga
//un valor superior a 6, si lo tiene "die1" se transforma en 1.
//Devuelve la suma de "die1" y "die2".
public int withoutDoubles(int die1, int die2, boolean noDoubles) {
if((noDoubles==true)&&(die1==die2))
if (die1<6)
die1=die1+1;
else
die1=1;
return die1+die2;
}



//25
//El método devuelve 0 si "a" y "b" son iguales,
//y devuelve el mayor en los demas casos excepto si
//el resto de dividir ambas variables por 5 es igual.
public int maxMod5(int a, int b) {
  if(a==b)
  return 0;
  if((a%5)==(b%5)){
  if (a>b)
  return b;
  else return a;
  }
  
  if (a>b)
  return a;
  else 
  return b;
    

}


//26
//Si "a","b" y "c" valen 2, el método devuelve 10.
//Si son iguales y no valen 2, el método devuelve 5.
//Si "a" es diferente a los otros dos valores, devuelve 1.
//En los demas casos devuelve 0.
public int redTicket(int a, int b, int c) {
if((a==2)&&(b==2)&&(c==2))
return 10;
if((a==b)&&(b==c))
return 5;
if((a!=b)&&(a!=c))
return 1;

return 0;
  
  
}


//27
//Si todas las variables son iguales, el método devuelve 20.
//Si todas son distintas devuelve 0.
//En el caso contrario devuelve 10.
public int greenTicket(int a, int b, int c) {
if ((a==b)&&(b==c))
return 20;

if ((a!=b)&&(b!=c)&&(a!=c))
return 0;

return 10;
  
}


//28
//Si la combinación de dos de la variables suma 10, el método devuelve 10.
//Si la suma de "a" + "b" es igual a alguna de las otras combinaciones + 10
//devuelve 5.
//Para todo lo demas devuelve 0.
public int blueTicket(int a, int b, int c) {
if(((a+b)==10)||((b+c)==10)||((c+a)==10))
return 10;
if(((a+b)==(b+c+10))||((a+b)==(a+c+10)))
  return 5;
  return 0;
}





//29
//Si alguno de los digitos de las dos variables coinciden  el método devuelve true
//si no devuelve false.
public boolean shareDigit(int a, int b) {
  if (((a/10)==(b%10))||((b/10)==(a%10))||((a/10)==(b/10))||((b%10)==(a%10)))
  return true;
    
    return false;
}



//30
//Éste metodo suma "a"+"b" y le da ese valor a "s".
//Si el número de dígitos de "s" es igual al de "a"
//devuelve "s", si no devuelve "a".
public int sumLimit(int a, int b) {
    int s= a+b;
String A= String.valueOf(a);
String B= String.valueOf(b);
String S= String.valueOf(s);
  
 
    if(A.length()==S.length()){
        return s;}
    else{ 
        return a;}
   

        }



//LOGIC2



//1
//El método devuelve true si jugando con los valores "big" (5) y "small" (1)
//y su número se puede obtener "goal"
//Si no devuelve false.

  public boolean makeBricks(int small, int big, int goal) {


while((goal-(big*5))<0)
big=big-1;

if (goal - (big*5)- small<=0)
return true;
return false;


}



//2
//El método devuelve la suma de las variables dadas excepto si alguna esta repetida
//en cuyo caso ambas no cuentan en la suma, o si todas se repiten, que devolverá 0.
  
public int loneSum(int a, int b, int c) {
 if ((a==b)&&(b==c))
  return 0;
  if (a==b)
  return c;
  if(b==c)
  return a;
  if (a==c)
  return b;
 
  return a+b+c;
}




//3
//El método devuelve la suma de las variables dadas, excepto si alguna de ellas es 13,
//en cuyo caso esa variable y todas las siguientes valen 0.
public int luckySum(int a, int b, int c) {
  
 
  
  if (a==13)
  return 0;
  
  if (b==13)
  return a;
  
  if (c==13)
  return a+b;
  
   return a+b+c;
}


//4
//El método devuelve la suma de las variables dadas.
//Si alguna de ellas se encuentra comprendida entre 13 y 15 o 16 y 19
//dicha variable vale 0.
public int noTeenSum(int a, int b, int c) {

if(((a>12)&&(a<15))||((a>16)&&(a<20)))
a=0;
if(((b>12)&&(b<15))||((b>16)&&(b<20)))
b=0;
if(((c>12)&&(c<15))||((c>16)&&(c<20)))
c=0;


return a+b+c;


}




//5
//El método devuelve la suma del redondeo de las variables dadas.
//El redondeo lo hace si la únidad es menor a 5, hacia la decena inferior,
//y si es 5 o mas, hacia la decena superior.
public int roundSum(int a, int b, int c) {
int A=((a/10)*10);
int B=((b/10)*10);
int C=((c/10)*10);
if ((a-A)<5)
a=A;
else
a=A+10;
if ((b-B)<5)
b=B;
else
b=B+10;
if ((c-C)<5)
c=C;
else
c=C+10;

return a+b+c;

  
}



//6
//Éste método devuelve true si "b" o "c" están a 1 de distancia de "a"
//y el otro está a 2 o mas de distancia de a.
//En caso contrario devuelve false.
public boolean closeFar(int a, int b, int c) {


if (((a-1)==c)&&((c-2)==b))  
return true;

if (((a-1)==b)&&((c-1)==a))  
return false;

if (((a+1)==b)&&((b+1)==c))  
return false;

if (((a-1)==b)&&((b-1)==c))  
return false;

if ((a-1)==c)  
return false;


return true;
      }



//7
//Éste método diferencia entre las dos variables dadas la que mas cerca se encuentre 
//de 21 sin pasarse y la devuelve.
//Si ambas variable son superiores a 21 devuelve 0.
public int blackjack(int a, int b) {
if (((21-a)<0)&&((21-b)<0))
return 0;
if ((21-a)<0)
return b;
if ((21-b)<0)
return a;
if((21-a)>(21-b))
return b;
return a;
  
}



//8
//Éste método ordena de menor a mayor las variables dadas y comprueba si existe 
//la misma distancia entre sus valores.
//Si es así devuelve true, si no false.
public boolean evenlySpaced(int a, int b, int c) {
if((a==b)&&(b==c))
return true;


  if ((a>b)&&(a>c)){
  
  if (b>c){
  
  if ((a-b)==(b-c))
  return true;}
  else
  if ((a-c)==(c-b))
  return true;
  }
  
    if ((b>a)&&(b>c)){
  
  if (a>c){
  
  if ((b-a)==(a-c))
  return true;}
  else
  if ((b-c)==(c-a))
  return true;
  }
  
    if ((c>b)&&(c>b)){
  
  if (b>a){
  
  if ((c-b)==(b-a))
  return true;}
  else
  if ((c-a)==(a-b))
  return true;
  }
  
  return false;
}




//9
//Al igual que el primero, éste método devuelve true si podemos alcanzar el valor de "goal"
//jugando con los valores de "small" (1) y "big" y su numero.
//Si es posible devuelve true, si no devuelve false.
public int makeChocolate(int small, int big, int goal) {
  


while((goal-(big*5))<0)
big=big-1;

if ((goal-(big*5))>small)
return -1;

return (goal-(big*5));
}





}